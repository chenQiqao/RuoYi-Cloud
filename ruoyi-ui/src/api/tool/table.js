import request from '@/utils/request'

// 查询生成表数据
export function pullUnreadMessage(query) {
  return request({
    url: '/system/pullUnreadMessage',
    method: 'post',
    params: query
  })
}

