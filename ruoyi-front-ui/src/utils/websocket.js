import SockJS from 'sockjs-client';
import Stomp from 'stompjs';
import {getToken} from '@/utils/auth'
import store from '@/store'

const socket = () => {
  //请求的起始地址，根据开发环境变量确定
  let baseUrl = process.env.VUE_APP_BASE_API;
  var header = {
    //可以不赋值，因为后面用SockJS来代替
    //brokerURL: 'ws://localhost:9527/dev-api/ws/',
    //获得客户端token的方法，把token放到请求头中
    connectHeaders: {"Authorization": 'Bearer ' + getToken()},
    reconnectDelay: 10000,//重连时间
    heartbeatIncoming: 4000,
    heartbeatOutgoing: 4000,
  };
  let stompClient = Stomp.over(new SockJS(baseUrl+'/chat-websocket'));
  // //用SockJS代替brokenURL

  return {
    log(v){
      console.log(v)
    },
    stompClient: stompClient,
    connect(callback) {
      let that=this;
      this.stompClient.connect({
        "Authorization": 'Bearer ' + getToken(),
        reconnectDelay: 10000,//重连时间
        heartbeatIncoming: 4000,
        heartbeatOutgoing: 4000,
      }, function (f) {
        that.log('Info: STOMP connection opened：'+f);
        callback();
      },function () {
        //断开处理
        that.log('Info: STOMP connection closed.');
      });
      //启动
    },
    close() {
      if (this.stompClient !== null) {
        this.stompClient.deactivate()
      }
    },
    //发送消息
    send(addr,to, msg) {
      this.stompClient.send("/webSocket"+addr, {}, JSON.stringify({
        message:msg,
        datetime:"2019-09-25",
        from:store.getters.name,
        to:to
      }));
    },
    //订阅消息
    subscribe(addr, callback) {
      this.stompClient.subscribe(addr, (res) => {
        var result = JSON.parse(res.body);
        callback(result);
      });
    }
  }
}
export default socket
