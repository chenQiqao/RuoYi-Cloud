package com.ruoyi.getway.impl.config.autoconfigure;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @description 接口实现工程自动配置
 * @author CHENQIAO
 * @date 2019年6月25日 下午12:01:41
 * @Copyright 版权所有 (c) CHENQIAO
 * @memo 无备注说明
 */
@Configuration
@ComponentScan({ "com.ruoyi.getway.impl" })
public class GetwayImplAutoConfiguration {

}
