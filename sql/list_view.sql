/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 80033
 Source Host           : localhost:3306
 Source Schema         : ry-cloud

 Target Server Type    : MySQL
 Target Server Version : 80033
 File Encoding         : 65001

 Date: 30/10/2023 22:56:09
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for list_view
-- ----------------------------
DROP TABLE IF EXISTS `list_view`;
CREATE TABLE `list_view`  (
  `view_id` bigint(0) NOT NULL COMMENT '查询id',
  `table_id` bigint(0) NULL DEFAULT NULL COMMENT '表id',
  `fields` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '查询列',
  `querys` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '查询条件',
  `buttons` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '按钮',
  `details` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL COMMENT '详情',
  `update_by` bigint(0) NULL DEFAULT NULL,
  `update_time` datetime(6) NULL DEFAULT NULL,
  `create_by` bigint(0) NULL DEFAULT NULL,
  `create_time` datetime(6) NULL DEFAULT NULL,
  PRIMARY KEY (`view_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '查询管理' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
