package com.ruoyi.chat.impl.controller;

import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.domain.Message;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.ExpireEnum;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.common.websockt.channelEnum.WebSocketChannelEnum;
import com.ruoyi.system.api.model.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class RedisMessageController {
    private static final Logger log = LoggerFactory.getLogger(RedisMessageController.class);

    private String topicName="topic-test";
 
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
 
    @Autowired
    private SimpUserRegistry userRegistry;

    @Autowired
    private RedisService redisService;

    @MessageMapping("/message")
    public void subscription(Message message, Principal principal) throws Exception {
        this.sendToUser(message, "/queue");
        System.out.println(principal.getName() + "发送了一条消息给：" + message.getTo());
    }

 
    /**
     * 给指定用户发送消息，并处理接收者不在线的情况
     * @param message 消息
     * @param destination 目的地
     */
    private void sendToUser(Message message, String destination){
        SimpUser simpUser = userRegistry.getUser(message.getTo());
 
        //如果接收者存在，则发送消息
        if(simpUser != null && StringUtils.isNoneBlank(simpUser.getName())){
            messagingTemplate.convertAndSendToUser(message.getTo(), destination, message.getMessage());
        }
        //如果接收者在线，则说明接收者连接了集群的其他节点，需要通知接收者连接的那个节点发送消息
        else if(redisService.isSetMember(Constants.REDIS_WEBSOCKET_USER_SET, message.getTo())){
            message.setChannelCode(WebSocketChannelEnum.CHAT.getCode());
            redisService.convertAndSend(topicName, message);
        }
        //否则将消息存储到redis，等用户上线后主动拉取未读消息
        else{
            //存储消息的Redis列表名
            String listKey = Constants.REDIS_UNREAD_MSG_PREFIX +":"+  message.getTo() + ":" + destination;
            log.info(MessageFormat.format("消息接收者{0}还未建立WebSocket连接，{1}发送的消息【{2}】将被存储到Redis的【{3}】列表中",  message.getTo(),  message.getFrom(),  message.getMessage(), listKey));
 
            //存储消息到Redis中
            redisService.addToListRight(listKey, ExpireEnum.UNREAD_MSG,JSON.toJSON( message));
        }
 
    }
 
 
    /**
     * 拉取指定监听路径的未读的WebSocket消息
     * @param destination 指定监听路径
     * @return java.util.Map<java.lang.String,java.lang.Object>
     */
    @PostMapping("/pullUnreadMessage")
    public Map<String, Object> pullUnreadMessage(String destination){
        Map<String, Object> result = new HashMap<>();
        try {
            LoginUser loginUser = SecurityUtils.getLoginUser();

            //存储消息的Redis列表名
            String listKey = Constants.REDIS_UNREAD_MSG_PREFIX + ":" +  loginUser.getUsername() + ":" + destination;
            //从Redis中拉取所有未读消息
            List<Object> messageList = redisService.rangeList(listKey, 0, -1);
 
            result.put("code", "200");
            result.put("result", messageList);
            if(messageList !=null && messageList.size() > 0){
                //删除Redis中的这个未读消息列表
                redisService.delete(listKey);
                //将数据添加到返回集，供前台页面展示
            }
        }catch (Exception e){
            result.put("code", "500");
            result.put("msg", e.getMessage());
        }
 
        return result;
    }
    // 用法一：
    @MessageMapping("/send")
    public void sendAll(@RequestParam String msg) {

        log.info("[发送消息]>>>> msg: {}", msg);

        // 发送消息给客户端
        // 第一个参数是浏览器中订阅消息的地址，第二个参数是消息本身
        messagingTemplate.convertAndSend("/topic/public", msg);
    }

    @MessageMapping("/chat.sendMessage")
    @SendTo("/topic/public")
    public Message sendMessage(@Payload Message  chatMessage) {
        return chatMessage;
    }
    @MessageMapping("/chatPlayAudio")
    public void playAudio(Message  message, Principal principal) throws Exception {
        try {
            InputStream inputStream= (InputStream) message.getMessage();
            byte[] buff = new byte[inputStream.available()];
            inputStream.read(buff, 0, inputStream.available());
            messagingTemplate.convertAndSendToUser(message.getTo(), "/playAudio", ByteBuffer.wrap(buff));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}