package com.ruoyi.chat.impl.Entity;

public class SignalEntity {
    private String type;

    private String msg;

    private int status;

    private Object data;

    public SignalEntity() {
    }

    public SignalEntity(String type, String msg, int status, Object data) {
        this.type = type;
        this.msg = msg;
        this.status = status;
        this.data = data;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
