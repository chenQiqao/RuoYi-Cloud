package com.ruoyi.chat.impl.controller;
import com.alibaba.fastjson2.JSON;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.domain.Message;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.ExpireEnum;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.security.utils.SecurityUtils;
import com.ruoyi.common.websockt.channelEnum.WebSocketChannelEnum;
import com.ruoyi.system.api.model.LoginUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.user.SimpUser;
import org.springframework.messaging.simp.user.SimpUserRegistry;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.io.InputStream;
import java.nio.ByteBuffer;
import java.security.Principal;
import java.text.MessageFormat;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
// 私信聊天的控制器
@RestController
public class WebSocketController {
    @Autowired
    private SimpMessagingTemplate messagingTemplate;
//    @Autowired
//    UserService userService;

    @MessageMapping("/api/chat")
    //在springmvc 中可以直接获得principal,principal 中包含当前用户的信息
    public void handleChat(Principal principal, Message messagePara) {

        String currentUserName = principal.getName();
        System.out.println(currentUserName);

        try {
            messagePara.setFrom(principal.getName());
            System.out.println("from" + messagePara.getFrom());
            messagingTemplate.convertAndSendToUser(messagePara.getTo(),
                    "/queue/notifications",
                    messagePara);
        } catch (Exception e) {
            // 打印异常
            e.printStackTrace();
        }
    }

//    @MessageMapping(value = "/api/entrance")
//    @SendTo(value = "/topic/users/list")//当服务端有消息时，会对订阅了@SendTo中的路径的浏览器发送消息
//    public User[] say(){
//        System.out.println("+++++++++++++");
//        User[] users = userService.getAllUser();
//        System.out.println(users[0].getUsername());
//        System.out.println("+++++++++++++");
//        return users;
//    }
}
