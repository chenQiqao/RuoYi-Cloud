package com.ruoyi.gen.impl.autoconfigure;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @description 接口实现工程自动配置
 * @author CHENQIAO
 * @date 2019年6月25日 下午12:01:41
 * @Copyright 版权所有 (c) CHENQIAO
 * @memo 无备注说明
 */
@Configuration
@MapperScan(value = "com.ruoyi.gen.impl.mapper")
@ComponentScan({ "com.ruoyi.gen.impl" })
public class GenImplAutoConfiguration {

}
