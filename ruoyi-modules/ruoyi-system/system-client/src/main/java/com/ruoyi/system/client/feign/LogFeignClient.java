package com.ruoyi.system.client.feign;

import com.ruoyi.system.client.feign.fallback.RemoteLogFallbackFactory;
import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.system.api.RemoteLogService;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Primary;

@FeignClient(contextId = "remoteLogService", value = ServiceNameConstants.SYSTEM_SERVICE, fallbackFactory = RemoteLogFallbackFactory.class)
@RefreshScope
public interface LogFeignClient extends RemoteLogService {

}
