package com.ruoyi.system.client.autoconfigure;

import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

/**
 * @description 支付客户端自动配置类
 * @author CHENQIAO
 * @date 2019年6月25日 上午9:24:49 
 * @Copyright 版权所有 (c) CHENQIAO
 * @memo 无备注说明
 */
@Configuration
@ComponentScan({"com.ruoyi.system.client.feign.fallback"})
@EnableFeignClients({"com.ruoyi.system.client.feign"})
public class SystemClientAutoConfiguration {

}
