package com.ruoyi.system.impl.controller;

import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.web.controller.BaseController;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.system.api.RemoteLogService;
import com.ruoyi.system.api.RemoteUserService;
import com.ruoyi.system.api.domain.SysLogininfor;
import com.ruoyi.system.api.domain.SysOperLog;
import com.ruoyi.system.impl.service.ISysLogininforService;
import com.ruoyi.system.impl.service.ISysOperLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class ClientLogController implements RemoteLogService {
    @Autowired
    private ISysLogininforService logininforService;
    @Autowired
    private RedisService redisService;
    @Autowired
    private ISysOperLogService operLogService;

    @Override
    public R<Boolean> saveLog(SysOperLog sysOperLog, String source) throws Exception {
        return R.ok( operLogService.insertOperlog(sysOperLog)>0);
    }

    @Override
    public R<Boolean> saveLogininfor(SysLogininfor sysLogininfor, String source) {
        return R.ok(logininforService.insertLogininfor(sysLogininfor)>0);
    }
}
