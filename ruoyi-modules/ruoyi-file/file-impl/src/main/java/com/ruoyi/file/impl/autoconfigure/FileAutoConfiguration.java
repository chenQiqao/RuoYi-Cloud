package com.ruoyi.file.impl.autoconfigure;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;

/**
 * @description 国外订单接口实现工程自动配置
 * @author CHENQIAO
 * @date 2019年6月25日 下午12:01:41
 * @Copyright 版权所有 (c) CHENQIAO
 * @memo 无备注说明
 */
@Configuration
@ComponentScan({ "com.ruoyi.file.impl" })
public class FileAutoConfiguration {

}
