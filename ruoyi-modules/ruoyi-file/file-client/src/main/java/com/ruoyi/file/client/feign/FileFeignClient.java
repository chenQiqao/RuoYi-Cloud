package com.ruoyi.file.client.feign;

import com.ruoyi.common.core.constant.ServiceNameConstants;
import com.ruoyi.file.client.feign.factory.RemoteFileFallbackFactory;
import com.ruoyi.system.api.RemoteFileService;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.cloud.openfeign.FeignClient;

/**
 * @description 支付微服务接口feign调用客户端
 * @author CHENQIAO
 * @date 2019年6月25日 上午9:16:08
 * @Copyright 版权所有 (c) CHENQIAO
 * @memo 无备注说明
 */
@FeignClient(contextId = "remoteFileService", value = ServiceNameConstants.FILE_SERVICE, fallbackFactory = RemoteFileFallbackFactory.class)
@RefreshScope
public interface FileFeignClient extends RemoteFileService {

}
