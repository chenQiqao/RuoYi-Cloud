package com.ruoyi.common.core.domain;
public class Message<T>{
    private String channelCode;
    private T message;//消息内容
    private String datetime;//发送时间
    private String from;//消息来源ID
    private String to;//发送消息给ID
    private String groupId;//群组

    public Message(String channelCode, String message, String datetime, String from, String to) {
        this.message = (T) message;
        this.channelCode =  channelCode;
        this.datetime = datetime;
        this.from = from;
        this.to = to;
    }

    public String getChannelCode() {
        return channelCode;
    }

    public void setChannelCode(String channelCode) {
        this.channelCode = channelCode;
    }

    public Message() {
    }

    public T getMessage() {
        return message;
    }

    public void setMessage(T message) {
        this.message = message;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }
}
