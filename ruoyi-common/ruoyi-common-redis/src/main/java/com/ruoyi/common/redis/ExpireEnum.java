package com.ruoyi.common.redis;

import java.util.concurrent.TimeUnit;

public enum ExpireEnum {
    UNREAD_MSG(100000000, TimeUnit.MINUTES);
    private final long time;
    private final TimeUnit timeUnit;

    ExpireEnum(long time, TimeUnit timeUnit) {
        this.time = time;
        this.timeUnit = timeUnit;
    }


    public long getTime() {
        return this.time;
    }
    public TimeUnit getTimeUnit() {
        return this.timeUnit;
    }
}
