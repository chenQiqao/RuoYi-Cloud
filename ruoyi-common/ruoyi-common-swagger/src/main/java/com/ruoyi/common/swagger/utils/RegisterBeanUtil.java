package com.ruoyi.common.swagger.utils;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.beans.factory.support.BeanDefinitionBuilder;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.DefaultListableBeanFactory;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.Bean;
import springfox.documentation.spring.web.plugins.Docket;

public class RegisterBeanUtil {

    /**

     */
    public static <T> T registerBean(DefaultListableBeanFactory registry, String name, Class<?> obj) {
        BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(obj);
        BeanDefinition beanDefinition = builder.getRawBeanDefinition();
        registry.registerBeanDefinition(name, beanDefinition);
        return  (T)registry.getBean(name, obj);
    }
}
