package com.ruoyi.common.swagger.properties;

public class ModeDefinition {
    private String packageInfo;
    private String path;

    public String getPackageInfo() {
        return packageInfo;
    }

    public void setPackageInfo(String packageInfo) {
        this.packageInfo = packageInfo;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
