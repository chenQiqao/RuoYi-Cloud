package com.ruoyi.common.swagger.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.ArrayList;
import java.util.List;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;

/**
 *
 *
 * @author ruoyi
 */
@Configuration
@ConfigurationProperties(prefix = "server.mode")
public class ModeProperties
{
   private Boolean enabled=false;

   public Boolean getEnabled() {
      return enabled;
   }

   public void setEnabled(Boolean enabled) {
      this.enabled = enabled;
   }

   private List<ModeDefinition> modeDefinitions;

   public List<ModeDefinition> getModeDefinitions() {
      return modeDefinitions;
   }

   public void setModeDefinitions(List<ModeDefinition> modeDefinitions) {
      this.modeDefinitions = modeDefinitions;
   }
}
