<p align="center">
	<img alt="logo" src="https://oscimg.oschina.net/oscnet/up-b99b286755aef70355a7084753f89cdb7c9.png">
</p>
<h1 align="center" style="margin: 30px 0 30px; font-weight: bold;">RuoYi v3.6.2</h1>
<h4 align="center">支持同一套业务代码内微服务和单体启动部署</h4>
<p align="center">
	<a href="https://gitee.com/chenQiqao/RuoYi-Cloud/stargazers"><img src="https://gitee.com/y_project/RuoYi-Cloud/badge/star.svg?theme=dark"></a>
	<a href="https://gitee.com/chenQiqao/RuoYi-Cloud"><img src="https://img.shields.io/badge/RuoYi-v3.6.2-brightgreen.svg"></a>
	<a href="https://gitee.com/chenQiqao/RuoYi-Cloud.git/blob/master/LICENSE"><img src="https://img.shields.io/github/license/mashape/apistatus.svg"></a>
</p>

## **支持同一套业务代码内微服务和单体启动部署**
###  为什么要同时支持微服务和单体启动部署呢，主要原因满足开发环境电脑性能和启动各种插件，满足正式发布环境中根据客户体量选择单体还是微服务，降低性能浪费和交付成本，那么我们开始使用ruoyi微服务版本做以下功能适配
1.  第一步拆分模块粒度,能够满足正常微服务和单体启动
* 拆分模块
~~~
├──web 单体启动模块
├──model
    ├── model-service 业务模块
    ├── model-api 对内外api模块
    ├── model-client 服务远程调用模块
    ├── model-boot 微服务启动聚合模块
~~~
* 使用自动配置扫描，配置模块类，在spring.factories注册暴露
~~~
/**
* @description 接口实现工程自动配置
* @author CHENQIAO
* @date 2019年6月25日 下午12:01:41
* @Copyright 版权所有 (c) CHENQIAO
* @memo 无备注说明
  */
  @Configuration
  @MapperScan(value = "com.ruoyi.gen.impl.mapper")
  @ComponentScan({ "com.ruoyi.gen.impl" })
  @EntityScan({ "com.ruoyi.gen.impl" })
  public class SystemImplAutoConfiguration {

}
~~~

2. 适配权鉴过滤器
   AuthFilter

3. 适配模块路由，由于getway网关  给我们访问服务加了服务前缀导致前端无法访问，做以下适配
* 继承RequestMappingHandlerMapping重写访问前缀,根据配置模块包路径和访问前缀名称
~~~
# Tomcat
server:
port: 8080
mode:
enabled: true #开启单机模式
modeDefinitions:
- packageInfo: com.ruoyi.system.impl
path: system
- packageInfo: com.ruoyi.auth.impl
path: auth
- packageInfo: com.ruoyi.gen.impl
path: code
- packageInfo: com.ruoyi.file.impl
path: file
- packageInfo: com.ruoyi.schedule.impl
path: job
~~~
~~~
public class ModeControlRequestMappingHandlerMapping extends RequestMappingHandlerMapping implements EnvironmentAware {
    private Environment environment;
    private ModeProperties modeProperties;
    @Override
    public void afterPropertiesSet() {
        // 初始化版本控制器类型集合
        super.afterPropertiesSet();
    }
    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        RequestMappingInfo info = super.getMappingForMethod(method, handlerType);
        if(modeProperties.getEnabled()) {
            if (info == null) return null;
            for (ModeDefinition modeDefinition : modeProperties.getModeDefinitions()) {
                boolean b = handlerType.getPackage().getName().startsWith(modeDefinition.getPackageInfo());
                if (handlerType.getPackage().getName().startsWith(modeDefinition.getPackageInfo())) {
                    RequestMappingInfo.Builder mutateBuilder = info.mutate();
                    info = mutateBuilder.paths(
                            info.getPatternValues().stream()
                                    .map(path -> modeDefinition.getPath() + path)
                                    .toArray(String[]::new)).build();
                }
            }
        }
        return info;
    }
    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        ConfigurationProperties annotation = ModeProperties.class.getAnnotation(ConfigurationProperties.class);
        Assert.notNull(annotation, "can not find annotation ConfigurationProperties");
        this.modeProperties = Binder.get(environment).bind(annotation.prefix(), ModeProperties.class).get();
    }
}
~~~

4.  适配图片验证码
* 使用tomcat的 Filter实现图片验证 源码查看 ValidateCodeFilter

5.  适配swagger模块分组
* 根据模块配置路径和前缀分组手动注入Docket的bean对象，由于Docket采用spring Plugin
  获取Docket配置bean是异步的 ,所以使用BeanDefinitionRegistryPostProcessor提前注入Docket 的bean对象 ，随便学习一下[spring Plugin](https://www.cnblogs.com/dreamroute/p/16276146.html)
~~~
@EnableConfigurationProperties(SwaggerProperties.class)
@ConditionalOnProperty(name = "swagger.enabled", matchIfMissing = true)
@Configuration
public class AutoBeanDefinitionRegistryPostProcessor implements EnvironmentAware, BeanDefinitionRegistryPostProcessor {
    private ModeProperties modeProperties;
    @Autowired
    private SwaggerProperties swaggerProperties;

    private Environment environment;

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {
        DefaultListableBeanFactory factory = (DefaultListableBeanFactory) registry;
        List<ModeDefinition> modeDefinitions1 = modeProperties.getModeDefinitions();
        swaggerProperties = factory.getBean(SwaggerProperties.class);
        List<ModeDefinition> modeDefinitions = modeProperties.getModeDefinitions();
        if(modeDefinitions==null||modeDefinitions.isEmpty()){
            modeDefinitions=new ArrayList<>();
            ModeDefinition p=new ModeDefinition();
            p.setPackageInfo("");
            p.setPath("default");
            modeDefinitions.add(p);
        }
        for (ModeDefinition modeConfig : modeDefinitions) {
            BeanDefinitionBuilder builder = BeanDefinitionBuilder.genericBeanDefinition(Docket.class, () -> {
                Docket autoFacDIBean = bulidDocket(modeConfig);
                return autoFacDIBean;
            });
            BeanDefinition beanDefinition = builder.getRawBeanDefinition();
            registry.registerBeanDefinition(modeConfig.getPath(), beanDefinition);
        }
    }

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory factory) throws BeansException {

    }

    /**
     * 默认的排除路径，排除Spring Boot默认的错误处理路径和端点
     */
    private static final List<String> DEFAULT_EXCLUDE_PATH = Arrays.asList("/error", "/actuator/**");

    private static final String BASE_PATH = "/**";

    //    @Bean
    public Docket bulidDocket(ModeDefinition modeConfig) {
        String basePackage = swaggerProperties.getBasePackage();
        String groupName = modeConfig.getPath();
        ;
        if (!StringUtils.hasLength(modeConfig.getPackageInfo())) {
            basePackage = modeConfig.getPackageInfo();
        }
        // base-path处理
        if (swaggerProperties.getBasePath().isEmpty()) {
            swaggerProperties.getBasePath().add(BASE_PATH);
        }
        // noinspection unchecked
        List<Predicate<String>> basePath = new ArrayList<Predicate<String>>();
        swaggerProperties.getBasePath().forEach(path -> basePath.add(PathSelectors.ant(path)));

        // exclude-path处理
        if (swaggerProperties.getExcludePath().isEmpty()) {
            swaggerProperties.getExcludePath().addAll(DEFAULT_EXCLUDE_PATH);
        }

        List<Predicate<String>> excludePath = new ArrayList<>();
        swaggerProperties.getExcludePath().forEach(path -> excludePath.add(PathSelectors.ant(path)));

        ApiSelectorBuilder builder = new Docket(DocumentationType.SWAGGER_2).host(swaggerProperties.getHost())
                .apiInfo(apiInfo(swaggerProperties)).select()
                .apis(RequestHandlerSelectors.basePackage(basePackage));

        swaggerProperties.getBasePath().forEach(p -> builder.paths(PathSelectors.ant(p)));
        swaggerProperties.getExcludePath().forEach(p -> builder.paths(PathSelectors.ant(p).negate()));
        Docket docket = builder.build().securitySchemes(securitySchemes()).securityContexts(securityContexts()).pathMapping("/");
        if (StringUtils.hasLength(groupName)) {
            docket.groupName(groupName);
        }
        return docket;

    }

    /**
     * 安全模式，这里指定token通过Authorization头请求头传递
     */
    private List<SecurityScheme> securitySchemes() {
        List<SecurityScheme> apiKeyList = new ArrayList<SecurityScheme>();
        apiKeyList.add(new ApiKey("Authorization", "Authorization", "header"));
        return apiKeyList;
    }

    /**
     * 安全上下文
     */
    private List<SecurityContext> securityContexts() {
        List<SecurityContext> securityContexts = new ArrayList<>();
        securityContexts.add(
                SecurityContext.builder()
                        .securityReferences(defaultAuth())
                        .operationSelector(o -> o.requestMappingPattern().matches("/.*"))
                        .build());
        return securityContexts;
    }

    /**
     * 默认的全局鉴权策略
     *
     * @return
     */
    private List<SecurityReference> defaultAuth() {
        AuthorizationScope authorizationScope = new AuthorizationScope("global", "accessEverything");
        AuthorizationScope[] authorizationScopes = new AuthorizationScope[1];
        authorizationScopes[0] = authorizationScope;
        List<SecurityReference> securityReferences = new ArrayList<>();
        securityReferences.add(new SecurityReference("Authorization", authorizationScopes));
        return securityReferences;
    }

    private ApiInfo apiInfo(SwaggerProperties swaggerProperties) {
        return new ApiInfoBuilder()
                .title(swaggerProperties.getTitle())
                .description(swaggerProperties.getDescription())
                .license(swaggerProperties.getLicense())
                .licenseUrl(swaggerProperties.getLicenseUrl())
                .termsOfServiceUrl(swaggerProperties.getTermsOfServiceUrl())
                .contact(new Contact(swaggerProperties.getContact().getName(), swaggerProperties.getContact().getUrl(), swaggerProperties.getContact().getEmail()))
                .version(swaggerProperties.getVersion())
                .build();
    }

    public void bindModeProperties() {
        ConfigurationProperties annotation = ModeProperties.class.getAnnotation(ConfigurationProperties.class);
        Assert.notNull(annotation, "can not find annotation ConfigurationProperties");
        this.modeProperties = Binder.get(environment).bind(annotation.prefix(), ModeProperties.class).get();
    }

    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        bindModeProperties();
    }
}
~~~
6.  适配事务
    待续................
## 演示图

<table>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/cd1f90be5f2684f4560c9519c0f2a232ee8.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/1cbcf0e6f257c7d3a063c0e3f2ff989e4b3.jpg"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8074972883b5ba0622e13246738ebba237a.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-9f88719cdfca9af2e58b352a20e23d43b12.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-39bf2584ec3a529b0d5a3b70d15c9b37646.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-4148b24f58660a9dc347761e4cf6162f28f.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-b2d62ceb95d2dd9b3fbe157bb70d26001e9.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d67451d308b7a79ad6819723396f7c3d77a.png"/></td>
    </tr>	 
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/5e8c387724954459291aafd5eb52b456f53.jpg"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/644e78da53c2e92a95dfda4f76e6d117c4b.jpg"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-8370a0d02977eebf6dbf854c8450293c937.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-49003ed83f60f633e7153609a53a2b644f7.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-d4fe726319ece268d4746602c39cffc0621.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-c195234bbcd30be6927f037a6755e6ab69c.png"/></td>
    </tr>
	<tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ece3fd37a3d4bb75a3926e905a3c5629055.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-92ffb7f3835855cff100fa0f754a6be0d99.png"/></td>
    </tr>
    <tr>
        <td><img src="https://oscimg.oschina.net/oscnet/up-ff9e3066561574aca73005c5730c6a41f15.png"/></td>
        <td><img src="https://oscimg.oschina.net/oscnet/up-5e4daac0bb59612c5038448acbcef235e3a.png"/></td>
    </tr>
</table>