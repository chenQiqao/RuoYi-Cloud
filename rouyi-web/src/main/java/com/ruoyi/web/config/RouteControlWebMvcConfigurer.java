package com.ruoyi.web.config;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcRegistrations;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.annotation.Order;
import org.springframework.core.Ordered;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

/**
 * @Description: 配置使用自定义路由url处理器
 * @author: chenqiao
 */
@Configuration
@Order(Ordered.HIGHEST_PRECEDENCE)
public class RouteControlWebMvcConfigurer implements WebMvcRegistrations
{

	@Override
    public RequestMappingHandlerMapping getRequestMappingHandlerMapping() {
        return new ModeControlRequestMappingHandlerMapping();
    }
}
