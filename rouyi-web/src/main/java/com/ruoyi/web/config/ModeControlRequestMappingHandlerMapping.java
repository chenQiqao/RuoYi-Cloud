package com.ruoyi.web.config;

import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.swagger.properties.ModeDefinition;
import com.ruoyi.common.swagger.properties.ModeProperties;
import com.ruoyi.common.swagger.utils.RegisterBeanUtil;
import org.springframework.boot.actuate.endpoint.ApiVersion;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.web.servlet.mvc.condition.*;
import org.springframework.web.servlet.mvc.method.RequestMappingInfo;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * 支持路由管理的RequestMapping注解映射处理器
 *
 */

public class ModeControlRequestMappingHandlerMapping extends RequestMappingHandlerMapping implements EnvironmentAware {
    private Environment environment;
    private ModeProperties modeProperties;
    @Override
    public void afterPropertiesSet() {
        // 初始化版本控制器类型集合
        super.afterPropertiesSet();
    }
    @Override
    protected RequestMappingInfo getMappingForMethod(Method method, Class<?> handlerType) {
        RequestMappingInfo info = super.getMappingForMethod(method, handlerType);
        if(modeProperties.getEnabled()) {
            if (info == null) return null;
            for (ModeDefinition modeDefinition : modeProperties.getModeDefinitions()) {
                boolean b = handlerType.getPackage().getName().startsWith(modeDefinition.getPackageInfo());
                if (handlerType.getPackage().getName().startsWith(modeDefinition.getPackageInfo())) {
                    RequestMappingInfo.Builder mutateBuilder = info.mutate();
                    info = mutateBuilder.paths(
                            info.getPatternValues().stream()
                                    .map(path -> modeDefinition.getPath() + path)
                                    .toArray(String[]::new)).build();
                }
            }
        }
        return info;
    }
    @Override
    public void setEnvironment(Environment environment) {
        this.environment = environment;
        ConfigurationProperties annotation = ModeProperties.class.getAnnotation(ConfigurationProperties.class);
        Assert.notNull(annotation, "can not find annotation ConfigurationProperties");
        this.modeProperties = Binder.get(environment).bind(annotation.prefix(), ModeProperties.class).get();
    }
}