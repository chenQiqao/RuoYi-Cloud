package com.ruoyi.web.config;


import com.ruoyi.getway.impl.config.properties.XssProperties;
//import com.ruoyi.web.filter.ValidateCodeFilter;
import com.ruoyi.web.filter.ValidateCodeFilter;
import com.ruoyi.web.filter.XssFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import javax.servlet.DispatcherType;
import java.util.HashMap;
import java.util.Map;

/**
 * Filter配置
 *
 * @author ruoyi
 */
@Configuration
public class FilterConfig
{
    @Autowired
    private XssProperties xss;
    private final static String[] VALIDATE_URL = new String[]{"/auth/login", "/auth/register"};

    @SuppressWarnings({ "rawtypes", "unchecked" })
    @Bean
    @ConditionalOnProperty(value = "security.xss.enabled", havingValue = "true")
    public FilterRegistrationBean xssFilterRegistration()
    {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.setFilter(new XssFilter());
        registration.addUrlPatterns(xss.getUrlPatterns().split(","));
        registration.setName("xssFilter");
        registration.setOrder(FilterRegistrationBean.HIGHEST_PRECEDENCE);
        Map<String, String> initParameters = new HashMap<String, String>();
        initParameters.put("excludes",String.join(",",xss.getExcludeUrls()));
        registration.setInitParameters(initParameters);
        return registration;
    }
//
//    @SuppressWarnings({ "rawtypes", "unchecked" })
//    @Bean
//    public FilterRegistrationBean someFilterRegistration()
//    {
//        FilterRegistrationBean registration = new FilterRegistrationBean();
//        registration.setFilter(new RepeatableFilter());
//        registration.addUrlPatterns("/*");
//        registration.setName("repeatableFilter");
//        registration.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
//        return registration;
//    }
    @Bean
    @ConditionalOnProperty(value = "security.captcha.enabled", havingValue = "true")
    public FilterRegistrationBean ValidateCodeRegistration()
    {
        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(new ValidateCodeFilter());
        registration.setDispatcherTypes(DispatcherType.REQUEST);
        registration.addUrlPatterns(VALIDATE_URL);
        registration.setName("validateCodeFilter");
        registration.setOrder(FilterRegistrationBean.LOWEST_PRECEDENCE);
        return registration;
    }
}
