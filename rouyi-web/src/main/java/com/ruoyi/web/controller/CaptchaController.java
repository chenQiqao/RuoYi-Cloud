package com.ruoyi.web.controller;

import com.ruoyi.common.core.web.domain.AjaxResult;
import com.ruoyi.getway.impl.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * 验证码操作处理
 *
 * @author ruoyi
 */
@RestController
public class CaptchaController {

    @Autowired
    private ValidateCodeService validateCodeService;

    /**
     * 生成验证码
     */
    @GetMapping("/code")
    public AjaxResult getCode(HttpServletResponse response) throws IOException {
        return validateCodeService.createCaptcha();
    }
}
