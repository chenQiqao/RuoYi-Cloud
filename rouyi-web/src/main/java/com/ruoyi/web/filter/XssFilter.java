package com.ruoyi.web.filter;

import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import com.ruoyi.getway.impl.config.properties.XssProperties;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Component;

import java.io.IOException;

/**
 * 跨站脚本过滤器
 *
 * @author ruoyi
 */
public class XssFilter implements Filter, Ordered {
    // 跨站脚本的 xss 配置，nacos自行添加

    private XssProperties xss;
    @Override
    public int getOrder() {
        return 1;
    }
    @Override
    public void init(FilterConfig filterConfig) throws ServletException
    {
        xss = SpringUtils.getBean(XssProperties.class);
    }
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        // xss开关未开启 或 通过nacos关闭，不过滤
        if (!xss.getEnabled()) {
            chain.doFilter(request, response);
            return;
        }
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;
        if (handleExcludeURL(req, resp)) {
            chain.doFilter(request, response);
            return;
        }
        CommonHttpServletRequestWrapper xssRequest = new CommonHttpServletRequestWrapper((HttpServletRequest) request);
        chain.doFilter(xssRequest, response);

    }

    private boolean handleExcludeURL(HttpServletRequest request, HttpServletResponse response) {
        String url = request.getServletPath();
        String method = request.getMethod();
        // GET DELETE 不过滤
        if (method == null || HttpMethod.GET.matches(method) || HttpMethod.DELETE.matches(method)) {
            return true;
        }
        return StringUtils.matches(url,  xss.getExcludeUrls());
    }

}
