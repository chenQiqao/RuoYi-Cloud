package com.ruoyi.web.filter;

import com.ruoyi.common.core.constant.CacheConstants;
import com.ruoyi.common.core.constant.HttpStatus;
import com.ruoyi.common.core.constant.SecurityConstants;
import com.ruoyi.common.core.constant.TokenConstants;
import com.ruoyi.common.core.utils.JwtUtils;
import com.ruoyi.common.core.utils.ServletUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.redis.service.RedisService;
import com.ruoyi.common.swagger.properties.ModeProperties;
import com.ruoyi.getway.impl.config.properties.IgnoreWhiteProperties;
import io.jsonwebtoken.Claims;
import org.apache.tomcat.util.http.MimeHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.context.properties.bind.Binder;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.Ordered;
import org.springframework.core.env.Environment;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.util.Assert;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.*;


@Component
public class AuthFilter implements Filter, Ordered {
    private static final Logger log = LoggerFactory.getLogger(AuthFilter.class);

    // 排除过滤的 uri 地址，nacos自行添加
    @Autowired
    private IgnoreWhiteProperties ignoreWhite;

    @Autowired
    private RedisService redisService;


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) servletRequest;
        HttpServletResponse response = (HttpServletResponse) servletResponse;
        CommonHttpServletRequestWrapper authRequest = new CommonHttpServletRequestWrapper(request);

        String url = request.getRequestURI();
        String token = getToken(request);
        // 跳过不需要验证的路径
        if (StringUtils.matches(url, ignoreWhite.getWhites())) {
            chain.doFilter(authRequest, response);
            return;
        }
        if (StringUtils.isEmpty(token)) {
            unauthorizedResponse(authRequest, response, "令牌不能为空");
            return;
        }
        Claims claims = JwtUtils.parseToken(token);
        if (claims == null) {
            unauthorizedResponse(authRequest, response, "令牌已过期或验证不正确！");
            return;
        }
        String userkey = JwtUtils.getUserKey(claims);
        boolean islogin = redisService.hasKey(getTokenKey(userkey));
        if (!islogin) {
            unauthorizedResponse(authRequest, response, "登录状态已过期");
            return;
        }
        String userid = JwtUtils.getUserId(claims);
        String username = JwtUtils.getUserName(claims);
        if (StringUtils.isEmpty(userid) || StringUtils.isEmpty(username)) {
            unauthorizedResponse(authRequest, response, "令牌验证失败");
            return;
        }
        // 设置用户信息到请求
        authRequest.putHeader(SecurityConstants.USER_KEY, userkey);
        authRequest.putHeader(SecurityConstants.DETAILS_USER_ID, userid);
        authRequest.putHeader(SecurityConstants.DETAILS_USERNAME, username);
        // 内部请求来源参数清除
        authRequest.removeHeader(SecurityConstants.FROM_SOURCE);
        chain.doFilter(authRequest, response);
        return;
    }

    private void addHeader(ServerHttpRequest.Builder mutate, String name, Object value) {
        if (value == null) {
            return;
        }
        String valueStr = value.toString();
        String valueEncode = ServletUtils.urlEncode(valueStr);
        mutate.header(name, valueEncode);
    }


    private void removeHeader(ServerHttpRequest.Builder mutate, String name) {
        mutate.headers(httpHeaders -> httpHeaders.remove(name)).build();
    }

    private void unauthorizedResponse(HttpServletRequest request, HttpServletResponse response, String msg) throws IOException {
        log.error("[鉴权异常处理]请求路径:{}:{}", request.getRequestURI(), msg);
        ServletUtils.responseWriter(response, msg, HttpStatus.UNAUTHORIZED);
    }

    private String getTokenKey(String token) {
        return CacheConstants.LOGIN_TOKEN_KEY + token;
    }


    private String getToken(HttpServletRequest request) {
        String token = request.getHeader(TokenConstants.AUTHENTICATION);
        // 如果前端设置了令牌前缀，则裁剪掉前缀
        if (StringUtils.isNotEmpty(token) && token.startsWith(TokenConstants.PREFIX)) {
            token = token.replaceFirst(TokenConstants.PREFIX, StringUtils.EMPTY);
        }
        return token;
    }

    @Override
    public int getOrder() {
        return -200;
    }



}

