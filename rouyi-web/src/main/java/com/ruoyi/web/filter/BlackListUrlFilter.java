//package com.ruoyi.web.filter;
//
//import com.ruoyi.common.core.constant.HttpStatus;
//import com.ruoyi.common.core.utils.ServletUtils;
//import com.ruoyi.common.swagger.properties.ModeProperties;
//import com.ruoyi.getway.impl.config.properties.IgnoreWhiteProperties;
//import com.ruoyi.system.impl.service.ISysConfigService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.context.properties.ConfigurationProperties;
//import org.springframework.boot.context.properties.bind.Binder;
//import org.springframework.core.Ordered;
//import org.springframework.core.env.Environment;
//import org.springframework.stereotype.Component;
//import org.springframework.util.Assert;
//
//import javax.servlet.*;
//import javax.servlet.http.HttpServletRequest;
//import javax.servlet.http.HttpServletResponse;
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.regex.Pattern;
//import java.util.stream.Collectors;
//
///**
// * 黑名单过滤器
// *
// * @author ruoyi
// */
//@Component
//public class BlackListUrlFilter implements Filter, Ordered{
//    @Autowired
//    private ISysConfigService configService;
//    private static BlackListConfig config;
//
//    @Override
//    public void init(FilterConfig filterConfig){
//        config=new BlackListConfig();
//        String blackStr = configService.selectConfigByKey("sys.login.blackIPList");
//        config.setBlacklistUrl(Arrays.stream(blackStr.split(",")).collect(Collectors.toList()));
//    }
//    @Override
//    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain chain) throws ServletException, IOException {
//        HttpServletRequest request = (HttpServletRequest) servletRequest;
//        HttpServletResponse response = (HttpServletResponse) servletResponse;
//        String url = request.getRequestURI();
//        if (config.matchBlacklist(url)) {
//             ServletUtils.responseWriter(response,"请求地址不允许访问", HttpStatus.UNAUTHORIZED);
//        }
//        //这里从缓存中读取
//        String blackStr = configService.selectConfigByKey("sys.login.blackIPList");
//        chain.doFilter(request, response);
//    }
//
//    @Override
//    public int getOrder() {
//        return 0;
//    }
//    public  static class BlackListConfig
//    {
//        private List<String> blacklistUrl;
//
//        private List<Pattern> blacklistUrlPattern = new ArrayList<>();
//
//        public boolean matchBlacklist(String url)
//        {
//            return !blacklistUrlPattern.isEmpty() && blacklistUrlPattern.stream().anyMatch(p -> p.matcher(url).find());
//        }
//
//        public List<String> getBlacklistUrl()
//        {
//            return blacklistUrl;
//        }
//
//        public void setBlacklistUrl(List<String> blacklistUrl)
//        {
//            this.blacklistUrl = blacklistUrl;
//            this.blacklistUrlPattern.clear();
//            this.blacklistUrl.forEach(url -> {
//                this.blacklistUrlPattern.add(Pattern.compile(url.replaceAll("\\*\\*", "(.*?)"), Pattern.CASE_INSENSITIVE));
//            });
//        }
//
//    }
//}
