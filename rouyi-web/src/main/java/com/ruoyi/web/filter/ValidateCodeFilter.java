package com.ruoyi.web.filter;

import com.alibaba.fastjson2.JSON;
import com.alibaba.fastjson2.JSONObject;
import com.ruoyi.common.core.constant.Constants;
import com.ruoyi.common.core.domain.R;
import com.ruoyi.common.core.utils.SpringUtils;
import com.ruoyi.common.core.utils.StringUtils;
import com.ruoyi.common.core.utils.http.HttpHelper;
import com.ruoyi.getway.impl.config.properties.CaptchaProperties;
import com.ruoyi.getway.impl.service.ValidateCodeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.core.Ordered;
import org.springframework.stereotype.Component;
import org.springframework.util.StreamUtils;

import javax.annotation.Resource;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

/**
 * 验证码过滤器
 *
 * @author ruoyi
 */

public class ValidateCodeFilter implements Filter, Ordered {
    private final static String[] VALIDATE_URL = new String[]{"/auth/login", "/auth/register"};

    private ValidateCodeService validateCodeService;

    private CaptchaProperties captchaProperties;

    private static final String CODE = "code";

    private static final String UUID = "uuid";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        validateCodeService = SpringUtils.getBean(ValidateCodeService.class);
        captchaProperties = SpringUtils.getBean(CaptchaProperties.class);
        Filter.super.init(filterConfig);

    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        // 非登录/注册请求或验证码关闭，不处理
        if (!StringUtils.containsAnyIgnoreCase(req.getRequestURI(), VALIDATE_URL) || !captchaProperties.getEnabled()) {
            chain.doFilter(request, resp);
            return;
        }
        try {
            ServletInputStream inputStream = request.getInputStream();
            String body = HttpHelper.getBodyString(inputStream);
            JSONObject obj = JSON.parseObject(body);
            validateCodeService.checkCaptcha(obj.getString(CODE), obj.getString(UUID));
            ServletRequest xssRequest =new ValidateCodeRequestWrapper(req,resp,body.getBytes(Constants.UTF8));
            chain.doFilter(xssRequest, response);
            return;
        } catch (Exception e) {
            response.setContentType("text/html; charset = utf-8");
            R<?> result = R.fail(R.FAIL, e.getMessage());
            PrintWriter printWriter = response.getWriter();
            printWriter.write(JSON.toJSONString(result));
        }
    }

    @Override
    public int getOrder() {
        return HIGHEST_PRECEDENCE;
    }
}
